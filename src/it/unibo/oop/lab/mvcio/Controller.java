package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;



/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + "output.dat";

    private File currentFile;
    /**
     * @param file the File to be set
     */
    public void setCurrentFile(final File file) {
        currentFile = file;
    }

    /**
     * @return returns the current file
     */
    public File getCurrentFile() {
        return currentFile;
    }
    /**
     * @return returns the path (as a String) to the current file
     */
    public String getPath() {
        return currentFile.getPath();
    }

    /**
     * @param o the object to be saved on the current file
     */
    public void save(final Object o) {
        try (
             OutputStream file = new FileOutputStream(PATH);
             OutputStream bstream = new BufferedOutputStream(file);
             ObjectOutputStream ostream = new ObjectOutputStream(bstream)
             ) {
             ostream .writeObject(o);
            } catch (IOException e1) { //Also throws this if the object is not serializable
                     e1.printStackTrace(); 
              }
        }

}
