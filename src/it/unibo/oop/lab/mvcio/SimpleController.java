package it.unibo.oop.lab.mvcio;

import java.util.LinkedList;
import java.util.List;

import it.unibo.oop.lab.mvc.Controller;

/**
 *
 *
 */
public class SimpleController implements Controller {

    private List<String> history = new LinkedList<String>();
    private String string;

    @Override
    public void setNext(final String next) throws NullPointerException {
        string = next;
    }

    @Override
    public String getNext() {
        return string;
    }

    @Override
    public List<String> getHistory() {
        return history;
    }

    @Override
    public void print() throws IllegalStateException {
        if (string == null) {
            throw new IllegalStateException();
        } else {
            System.out.println(string);
            history.add(string); 
        }

    }

}
