package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import it.unibo.oop.lab.mvcio.Controller;
import it.unibo.oop.lab.mvcio.SimpleController;


/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
    private static final int PROPORTION = 50;
    private final JFrame frame = new JFrame();
    private final SimpleController controller = new SimpleController();
    private final Controller fileController = new Controller();

    /**
     * builds a new {@link SimpleGUIWithFileChooser}.
     * @param title the title of the window
     */
    public SimpleGUIWithFileChooser(final String title) {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setTitle(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel canvasCenter = new JPanel(new GridBagLayout());
        final JPanel canvasSouth = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final JPanel canvasNorth = new JPanel(new BorderLayout());

        final JTextField textField = new JTextField("Insert lines for printing", 20);
        final JTextField fileText = new JTextField();
        fileText.setEditable(false);

        final JTextArea textArea = new JTextArea("History text field");
        textArea.setLineWrap(true);
        final JScrollPane scroll = new JScrollPane(textArea);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        final JButton print = new JButton("Print");
        print.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                controller.setNext(textField.getText());
                controller.print();
            }
        });

        final JButton show = new JButton("show");
        show.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                textArea.replaceRange("", 0, textArea.getColumns());
                for (String str : controller.getHistory()) {
                    textArea.append(str);
                    textArea.append("    ");
                }
            }
        });

        final JButton browse = new JButton("Browse..");
        show.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "JPG & GIF Images", "jpg", "gif");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(frame);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                   System.out.println("You chose to open this file: " 
                + chooser.getSelectedFile().getName());
                } else if (returnVal != JFileChooser.CANCEL_OPTION) {
                  JOptionPane.showMessageDialog(frame, "An error has occurred!");
                }
            }
        });
        frame.add(canvasCenter, BorderLayout.CENTER);
        frame.add(canvasSouth, BorderLayout.SOUTH);
        frame.add(canvasNorth, BorderLayout.NORTH);
        final GridBagConstraints cnst = new GridBagConstraints();
        cnst.gridy = 0; // 1-a riga
        cnst.insets = new Insets(3, 3, 3, 3); // spazio attorno al comp .
        cnst.fill = GridBagConstraints.HORIZONTAL; // estensione in orizzont .
        canvasCenter.add(textField, cnst);
        cnst.gridy++;
        textArea.setRows(sh / PROPORTION);
        canvasCenter.add(scroll, cnst);
        cnst.gridy++;
        textArea.setSize(sw / 3, sh / 3);
        canvasSouth.add(print);
        canvasSouth.add(show);
        canvasNorth.add(browse);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }

    /**
     * 
     */
    public void display() {
        frame.setVisible(true);
    }

    /**
     * @param args ignored
     */
    public static void main(final String... args) {
        new SimpleGUIWithFileChooser("Simple GUI").display();
     }
}
