package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import it.unibo.oop.lab.mvcio.SimpleController;


/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private static final int PROPORTION = 50;
    private final JFrame frame = new JFrame();
    private final SimpleController controller = new SimpleController();
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show controller.getHistory()". 
     * SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show controller.getHistory()" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */

    /**
     * builds a new {@link SimpleGUI}.
     * @param title the title of the window
     */
    public SimpleGUI(final String title) {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setTitle(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel canvasNorth = new JPanel(new GridBagLayout());

        final JPanel canvasSouth = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        final JTextField textField = new JTextField("Insert lines for printing", 20);

        final JTextArea textArea = new JTextArea("");
        textArea.setLineWrap(true);
        final JScrollPane scroll = new JScrollPane(textArea);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        final JButton print = new JButton("Print");
        print.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                controller.setNext(textField.getText());
                controller.print();
            }
        });

        final JButton show = new JButton("show");
        show.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                textArea.replaceRange("", 0, textArea.getColumns());
                for (String str : controller.getHistory()) {
                    textArea.append(str);
                    textArea.append("    ");
                }
            }
        });

        frame.add(canvasNorth, BorderLayout.NORTH);
        frame.add(canvasSouth, BorderLayout.SOUTH);
        final GridBagConstraints cnst = new GridBagConstraints();
        cnst.gridy = 0; // 1-a riga
        cnst.insets = new Insets(3, 3, 3, 3); // spazio attorno al comp .
        cnst.fill = GridBagConstraints.HORIZONTAL; // estensione in orizzont .
        canvasNorth.add(textField, cnst);
        cnst.gridy++;
        textArea.setRows(sh / PROPORTION);
        canvasNorth.add(scroll, cnst);
        cnst.gridy++;
        textArea.setSize(sw / 3, sh / 3);
        canvasSouth.add(print);
        canvasSouth.add(show);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }

    /**
     * 
     */
    public void display() {
        frame.setVisible(true);
    }

    /**
     * @param args ignored
     */
    public static void main(final String... args) {
        new SimpleGUI("Simple GUI").display();
     }
}
